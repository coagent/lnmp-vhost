#!/bin/bash

# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: You must be root to run this script, use sudo sh $0"
    exit 1
fi

clear

if [ "$1" != "--help" ]; then

	domain="www.domain.com"
	echo "Please input domain:"
	read -p "(Default domain: www.domain.com):" domain
	if [ "$domain" = "" ]; then
		domain="www.domain.com"
	fi
	if [ ! -f "/usr/local/nginx/conf/vhost/$domain.conf" ]; then
	echo "==========================="
	echo "domain=$domain"
	echo "===========================" 
	else
	echo "==========================="
	echo "$domain is exist!"
	echo "==========================="	
	fi
	
	echo "Do you want to add more domain name? (y/n)"
	read add_more_domainame

	if [ "$add_more_domainame" == 'y' ]; then

	  echo "Type domainname,example(bbs.vpser.net forums.vpser.net luntan.vpser.net):"
	  read moredomain
          echo "==========================="
          echo domain list="$moredomain"
          echo "==========================="
	  moredomainame=" $moredomain"
	fi

	vhostdir="/srv/www/vhosts/$domain"
	echo "==========================="
	echo Virtual Host Directory="$vhostdir"
	echo "==========================="

	echo "==========================="
	echo "Allow Rewrite rule? (y/n)"
	echo "==========================="
	read allow_rewrite

	if [ "$allow_rewrite" == 'n' ]; then
		rewrite="none"
	else
		rewrite="other"
		echo "Please input the rewrite of programme :"
		echo "wordpress,discuz,typecho,sablog,dabr rewrite was exist."
		read -p "(Default rewrite: other):" rewrite
		if [ "$rewrite" = "" ]; then
			rewrite="other"
		fi
	fi
	echo "==========================="
	echo You choose rewrite="$rewrite"
	echo "==========================="

	echo "==========================="
	echo "Allow access_log? (y/n)"
	echo "==========================="
	read access_log

	if [ "$access_log" == 'n' ]; then
	  al="access_log off;"
	else
	  #echo "Type access_log name(Default access log file:$domain.log):"
	  #read al_name
	  #if [ "$al_name" = "" ]; then
		al_name="$domain"
	  #fi
	  #al="log_format  $al_name  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
          #   '\$status \$body_bytes_sent "\$http_referer" '
          #   '"\$http_user_agent" \$http_x_forwarded_for';
		#access_log  /srv/www/logs/$al_name.log  $al_name;"
	  al="access_log  /srv/www/logs/$al_name.log;"
	echo "==========================="
	echo You access log file="$al_name.log"
	echo "==========================="
	fi

	read -p "Please enter your password:" domain_user_password

	get_char()
	{
	SAVEDSTTY=`stty -g`
	stty -echo
	stty cbreak
	dd if=/dev/tty bs=1 count=1 2> /dev/null
	stty -raw
	stty echo
	stty $SAVEDSTTY
	}
	echo ""
	echo "Press any key to start create virtul host..."
	char=`get_char`

if [ ! -d /usr/local/php/etc/vhosts ]; then
        mkdir -p /usr/local/php/etc/vhosts
fi

if [ ! -d /usr/local/nginx/conf/vhost ]; then
	mkdir /usr/local/nginx/conf/vhost
fi

if [ ! -d /srv/www/vhosts ]; then
        mkdir -p /srv/www/vhosts
fi

if [ ! -d /srv/www/logs ]; then
        mkdir -p /srv/www/logs
fi



echo "Create Virtul Host directory......"
logsdir=$vhostdir/logs
wwwdir=$vhostdir/wwwroot
etcdir=$vhostdir/etc
bakdir=$vhostdir/backup
ssldir=$vhostdir/ssl
tmpdir=$vhostdir/tmp

mkdir -p $vhostdir
mkdir -p $logsdir
mkdir -p $wwwdir
mkdir -p $etcdir
mkdir -p $bakdir
mkdir -p $ssldir
mkdir -p $tmpdir

useradd  $domain -d $vhostdir -G users -M -s /bin/false
echo "$domain:$domain_user_password" | chpasswd

touch /srv/www/logs/$al_name.log
ln -s /srv/www/logs/$al_name.log $logsdir/$al_name.log

if [ ! -f /usr/local/nginx/conf/$rewrite.conf ]; then
  echo "Create Virtul Host ReWrite file......"
	touch /usr/local/nginx/conf/$rewrite.conf
	echo "Create rewirte file successful,now you can add rewrite rule into /usr/local/nginx/conf/$rewrite.conf."
else
	echo "You select the exist rewrite rule:/usr/local/nginx/conf/$rewrite.conf"
fi

cat >/usr/local/php/etc/vhosts/$domain.conf<<eof

[$domain]
listen = /tmp/$domain.sock
user = $domain
group = $domain
pm = dynamic
pm.max_children = 5
pm.start_servers = 3
pm.min_spare_servers = 3
pm.max_spare_servers = 5

eof

cat >/usr/local/nginx/conf/vhost/$domain.conf<<eof
server
	{
		listen       80;
		server_name $domain $moredomainame;

		index index.html index.htm index.php default.html default.htm default.php;
		root  $wwwdir;
		error_page 400 /error/400.html;
	        error_page 401 /error/401.html;
        	error_page 403 /error/403.html;
        	error_page 404 /error/404.html;
        	error_page 405 /error/405.html;
        	error_page 500 /error/500.html;
        	error_page 502 /error/502.html;
        	error_page 503 /error/503.html;
		
		include $rewrite.conf;
		
		## Disable .htaccess and other hidden files
        	location ~ /\. {
            		deny all;
            		access_log off;
            		log_not_found off;
       		 }

        	location = /favicon.ico {
            		log_not_found off;
            		access_log off;
       		 }

        	location = /robots.txt {
            		allow all;
            		log_not_found off;
            		access_log off;
       		 }


		location ~ .*\.(php|php5)?$
			{
				fastcgi_pass  unix:/tmp/$domain.sock;
				fastcgi_index index.php;
				include fcgi.conf;
			}

		location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
			{
				expires      30d;
			}

		location ~ .*\.(js|css)?$
			{
				expires      12h;
			}

		$al
	}
eof

ln -s /usr/local/nginx/conf/vhost/$domain.conf $etcdir/$domain.conf
cp -R template/* $wwwdir/
echo "set permissions of Virtual Host directory......"

#bakdir etcdir logsdir ssldir tmpdir wwwdir
chown -R $domain:root $vhostdir
chmod -R 755 $vhostdir
chmod -R 755 $wwwdir

#Backup Temp DIR: Domain owner, root
chmod -R 740 $bakdir $tmpdir

#config logs ssl: root only
chown -R root:root $etcdir $logsdir $ssldir
chmod -R 644 $etcdir $logsdir $ssldir

echo "Test Nginx configure file......"
/usr/local/nginx/sbin/nginx -t
echo ""
echo "Restart Nginx......"
/usr/local/nginx/sbin/nginx -s reload

echo ""
echo "Your domain:$domain"
echo "Directory of $domain:$vhostdir"
echo ""
fi
